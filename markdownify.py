#!/usr/bin/python
import os
import sys
import requests
from bs4 import BeautifulSoup
import urllib2
from urlparse import urlparse

def get_title(url):
    soup = BeautifulSoup(urllib2.urlopen(url))
    if soup.title is not None and len(soup.title.string)>0:
        return soup.title.string
    else:
        urlparsed = urlparse(url)
        domain = '{uri.netloc} '.format(uri=urlparsed)
        link_title = ('{uri.path}'.format(uri=urlparsed))[1:]
        string = str(domain) + str(link_title)
        return (string)
 
def markdownify(payload):
    r = requests.get("http://fuckyeahmarkdown.com/go", params=payload)

    if (r.status_code == 200):
        actual_title = get_title(payload["u"])
        actual_title = actual_title.replace('\\', '-')
        actual_title = actual_title.replace('/', '-')
        
        if len(actual_title) > 0 or (actual_title):
            with open(((actual_title)+".txt"), 'wb+') as fd:
                for chunk in r.content:
                    fd.write(chunk)
            print ("Done!")
        else:
            print ("error parsing")

def main(): 
    # os.environ["KMVAR_SafariURLBookmark"] = "http://intermediatepythonista.com/the-function-ii-function-decorators" 
    if len(os.environ["KMVAR_SafariURLBookmark"]) > 0:
        if os.environ["KMVAR_SafariURLBookmark"].startswith("http"):
            url = str(os.environ["KMVAR_SafariURLBookmark"])

    payload = {
        "u" : url
    }

    markdownify(payload)

if __name__ == "__main__":
    main()
